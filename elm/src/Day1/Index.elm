module Day1.Index exposing (..)

import Utils exposing (divideBy, subtract)


parseInput : String -> List Int
parseInput input =
    input
        |> String.split "\n"
        |> List.foldr
            (\cur acc ->
                let
                    myInt =
                        String.toInt cur
                in
                case myInt of
                    Just a ->
                        a :: acc

                    Nothing ->
                        acc
            )
            []


calculateFuel : Int -> Int
calculateFuel mass =
    mass |> divideBy 3 |> floor |> subtract 2


recursiveCalculateFuel : ( Int, Int ) -> ( Int, Int )
recursiveCalculateFuel values =
    let
        ( acc, cur ) =
            values

        res =
            calculateFuel cur

        negativeFuel =
            res < 0
    in
    if negativeFuel then
        ( acc, res )

    else
        recursiveCalculateFuel ( acc + res, res )


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> List.map calculateFuel
        |> List.foldl (+) 0


solvePart2 : String -> Int
solvePart2 input =
    input
        |> parseInput
        |> List.map (\x -> recursiveCalculateFuel ( 0, x ) |> Tuple.first)
        |> List.foldl (+) 0
