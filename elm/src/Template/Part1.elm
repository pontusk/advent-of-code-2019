module Day1.Part1 exposing (..)


parseInput : String -> List Int
parseInput input =
    input
        |> String.split "\n"
        |> List.foldr
            (\cur acc ->
                let
                    myInt =
                        String.toInt cur
                in
                case myInt of
                    Just a ->
                        a :: acc

                    _ ->
                        acc
            )
            []


countHigherThanPrev : List Int -> Int
countHigherThanPrev numbers =
    numbers
        |> List.foldl
            (\cur acc ->
                let
                    ( prev, sum ) =
                        acc

                    greaterThenPrev =
                        cur > prev && prev /= 0
                in
                if greaterThenPrev then
                    ( cur, 1 + sum )

                else
                    ( cur, sum )
            )
            ( 0, 0 )
        |> Tuple.second
