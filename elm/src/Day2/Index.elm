module Day2.Index exposing (..)

import Array exposing (Array)
import Utils exposing (getAtIndex)


parseInput : String -> List Int
parseInput input =
    input
        |> String.split ","
        |> List.foldr
            (\cur acc ->
                let
                    myInt =
                        cur |> String.trim |> String.toInt
                in
                case myInt of
                    Just a ->
                        a :: acc

                    _ ->
                        acc
            )
            []


type alias Model =
    { memory : Array Int
    , addr : Int
    , noun : Int
    , verb : Int
    , firstOp : Int
    }


createModel : Array Int -> Model
createModel memory =
    { memory = memory
    , addr = 0
    , noun = getAtIndex 1 memory
    , verb = getAtIndex 2 memory
    , firstOp = getAtIndex 0 memory
    }


runProgram : Model -> Model
runProgram model =
    let
        { memory, addr } =
            model

        cur =
            getAtIndex addr memory
    in
    case cur of
        99 ->
            model

        1 ->
            runProgram (updateMemory model (+))

        2 ->
            runProgram (updateMemory model (*))

        _ ->
            runProgram { model | addr = addr + 1 }


updateInputs : Int -> Int -> Model -> Model
updateInputs noun verb model =
    { model
        | memory = model.memory |> Array.set 1 noun |> Array.set 2 verb
        , noun = noun
        , verb = verb
    }


updateMemory : Model -> (Int -> Int -> Int) -> Model
updateMemory model op =
    let
        { addr, memory } =
            model

        addr1 =
            getAtIndex (addr + 1) memory

        addr2 =
            getAtIndex (addr + 2) memory

        addr3 =
            getAtIndex (addr + 3) memory

        noun =
            getAtIndex addr1 memory

        verb =
            getAtIndex addr2 memory

        res =
            op noun verb
    in
    { model | memory = Array.set addr3 res memory, addr = addr + 4 }


recurse : Int -> Int -> Model -> Model
recurse noun verb model =
    let
        res =
            model |> updateInputs noun verb |> runProgram

        dif =
            19690720 - getAtIndex 0 res.memory

        ( newNoun, newVerb ) =
            incrementInputs dif model
    in
    if dif == 0 then
        model

    else
        recurse newNoun newVerb model


incrementInputs : Int -> Model -> ( Int, Int )
incrementInputs dif model =
    let
        d =
            toFloat dif
    in
    if model.firstOp == 1 then
        ( round (d / 2), round(d / 2) )

    else
        ( round (sqrt d), round  (sqrt d) )


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> Array.fromList
        |> createModel
        |> updateInputs 12 2
        |> runProgram
        |> (\x -> getAtIndex 0 x.memory)


solvePart2 : String -> Int
solvePart2 input =
    input
        |> parseInput
        |> Array.fromList
        |> createModel
        |> updateInputs 0 0
        |> runProgram
        |> Debug.log "model"
        |> (\x -> getAtIndex 0 x.memory)
