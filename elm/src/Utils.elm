module Utils exposing (..)
import Array exposing (Array)


stringToInt : String -> Int
stringToInt str =
    Maybe.withDefault 0 (String.toInt str)


divideBy : Int -> Int -> Float
divideBy a b =
    toFloat b / toFloat a


subtract : Int -> Int -> Int
subtract a b =
    b - a

getAtIndex : Int -> Array Int -> Int
getAtIndex index arr =
    Maybe.withDefault 0 (Array.get index arr)
