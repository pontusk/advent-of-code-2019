module Day1.Part2Test exposing (..)

import Day1.Index exposing (recursiveCalculateFuel, solvePart2)
import Day1.Inputs exposing (..)
import Expect exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day 1 Part 2"
        [ describe "recursiveCalculateFuel"
            [ test "Recursively calculate the fuel cost" <|
                \_ ->
                    ( 0, 1969 )
                        |> recursiveCalculateFuel
                        |> Tuple.first
                        |> Expect.equal 966
            ]
        , describe "answer"
            [ test "The answer is correct" <|
                \_ ->
                    finalInput
                        |> solvePart2
                        |> Expect.equal 5194864
            ]
        ]
