module Day1.Part1Test exposing (..)

import Day1.Index exposing (calculateFuel, parseInput, solvePart1)
import Day1.Inputs exposing (finalInput, input, smallInput)
import Expect exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day 1 Part 1"
        [ describe "parseInput"
            [ test "Make a list of numbers" <|
                \_ ->
                    smallInput
                        |> parseInput
                        |> Expect.equal [ 12, 14 ]
            ]
        , describe "calculateFuel"
            [ test "Divide by 3, round down and subtract 2" <|
                \_ ->
                    input
                        |> parseInput
                        |> List.map calculateFuel
                        |> Expect.equal [ 2, 2, 654, 33583 ]
            ]
        , describe "fuelSum"
            [ test "Sum up all the fuel requirements" <|
                \_ ->
                    smallInput
                        |> parseInput
                        |> List.map calculateFuel
                        |> List.foldl (+) 0
                        |> Expect.equal 4
            ]
        , describe "answer"
            [ test "The answer is correct" <|
                \_ ->
                    finalInput
                        |> solvePart1
                        |> Expect.equal 3465154
            ]
        ]
