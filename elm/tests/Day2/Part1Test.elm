module Day2.Part1Test exposing (..)

import Array exposing (..)
import Day2.Index exposing ( parseInput, solvePart1)
import Day2.Inputs exposing (finalInput, smallInput)
import Expect exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day 2 Part 1"
        [ describe "parseInput"
            [ test "Make a list of numbers" <|
                \_ ->
                    smallInput
                        |> parseInput
                        |> Expect.equal [ 1, 0, 0, 0, 99 ]
            ]
        , describe "answer"
            [ test "The answer is correct" <|
                \_ ->
                    finalInput
                        |> solvePart1
                        |> Expect.equal 3224742
            ]
        ]
