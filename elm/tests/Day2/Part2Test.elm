module Day2.Part2Test exposing (..)

import Array exposing (..)
import Day2.Index exposing (..)
import Day2.Inputs exposing (finalInput)
import Expect exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day 2 Part 2"
        [ describe "incrementInputs"
            [ test "It increments inputs" <|
                \_ ->
                    incrementInputs 20 { addr = 0, memory = Array.fromList [ 2, 0, 0, 3, 99 ], firstOp = 2, noun = 0, verb = 0 }
                        |> Expect.equal (10, 9)
            , describe "answer"
                [ test "The answer is correct" <|
                    \_ ->
                        finalInput
                            |> solvePart2
                            |> Expect.equal 19690720
                ]
            ]
        ]
